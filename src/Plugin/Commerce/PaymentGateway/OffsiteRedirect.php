<?php

namespace Drupal\commerce_coinbase\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "coinbase_offsite",
 *   label = "Coinbase (Off-site)",
 *   display_label = "Coinbase",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_coinbase\PluginForm\OffsiteRedirect\PaymentOffsiteForm",
 *   },
 *   payment_method_types = {"crypto_wallet"},
 *   requires_billing_information = FALSE,
 * )
 */
class OffsiteRedirect extends OffsitePaymentGatewayBase {

  /**
   * @var \Drupal\Core\Routing\ResettableStackedRouteMatchInterface;
   */
  protected $routeMatch;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }


  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => '',
      'secret' => '',
      'charge_name' => '',
      'charge_description' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
      '#description' => $this->t('You can get API Key there <a href="@url" target="_blank">@url</a>', ['@url' => Url::fromUri('https://beta.commerce.coinbase.com/settings/security')->toString()]),
    ];

    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret'),
      '#default_value' => $this->configuration['secret'],
      '#required' => TRUE,
      '#description' => $this->t('You can get Shared Secret there <a href="@url" target="_blank">@url</a>', ['@url' => Url::fromUri('https://beta.commerce.coinbase.com/settings/notifications')->toString()]),
    ];
    $payment_gateway = $this->routeMatch->getParameter('commerce_payment_gateway');
    if ($payment_gateway) {
      $webhook_url = Url::fromRoute('commerce_coinbase.webhook', ['commerce_payment_gateway' => 'coinbase'], ['absolute' => TRUE])->toString();
    }
    else {
      $webhook_url = $this->t('Will be available after saving');
    }
    $form['info'] = [
      '#type' => 'item',
      '#title' => 'Webhook endpoint',
      '#markup' => $webhook_url,
      '#description' => $this->t('You need to configure Webhook Endpoint there <a href="@url" target="_blank">@url</a>', ['@url' => Url::fromUri('https://beta.commerce.coinbase.com/settings/notifications')->toString()]),
    ];

    $form['charge_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $this->configuration['charge_name'] ?: 'Order #[commerce_order:order_id]',
      '#required' => TRUE,
      '#description' => $this->t('Charge name, 100 characters or less. This field support tokens.'),
    ];

    $form['charge_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $this->configuration['charge_description'] ?: 'Order #[commerce_order:order_id] at [site:name]',
      '#required' => TRUE,
      '#description' => $this->t('More detailed description of the charge, 200 characters or less. This field support tokens.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_key'] = $values['api_key'];
      $this->configuration['secret'] = $values['secret'];
      $this->configuration['charge_name'] = $values['charge_name'];
      $this->configuration['charge_description'] = $values['charge_description'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // @todo Add examples of request validation.
    // Note: Since requires_billing_information is FALSE, the order is
    // not guaranteed to have a billing profile. Confirm that
    // $order->getBillingProfile() is not NULL before trying to use it.

    // $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    // $payment = $payment_storage->create([
    //   'state' => 'completed',
    //   'amount' => $order->getBalance(),
    //   'payment_gateway' => $this->parentEntity->id(),
    //   'order_id' => $order->id(),
    //   'remote_id' => $request->query->get('txn_id'),
    //   'remote_state' => $request->query->get('payment_status'),
    // ]);
    // $payment->save();
  }

}
