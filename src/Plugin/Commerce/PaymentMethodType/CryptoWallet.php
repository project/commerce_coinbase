<?php

namespace Drupal\commerce_coinbase\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the crypto currency payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "crypto_wallet",
 *   label = @Translation("Crypto Wallet"),
 * )
 */
class CryptoWallet extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $args = [
      '@number' => '123',//$payment_method->card_number->value,
    ];
    return $this->t('CryptoWallet ending in @card_number', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['crypto_type'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('The cryptocurrency type.'))
      ->setRequired(TRUE);

    $fields['crypto_number'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Number'))
      ->setDescription(t('The last few digits of the credit card number'))
      ->setRequired(TRUE);

    return $fields;
  }

}
