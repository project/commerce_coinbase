<?php

namespace Drupal\commerce_coinbase\Controller;

use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_coinbase\CoinbaseApi;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Returns responses for Commerce Coinbase routes.
 */
class CommerceCoinbaseController extends ControllerBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The coinbase API service.
   *
   * @var \Drupal\commerce_coinbase\CoinbaseApi
   */
  protected $api;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface|null
   */
  protected $timeService;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The controller constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(Request $request, LoggerChannelInterface $logger_channel, SerializerInterface $serializer, EventDispatcherInterface $event_dispatcher, CoinbaseApi $coinbase_api, TimeInterface $time_service, EntityTypeManagerInterface $entity_type_manager) {
    $this->request = $request;
    $this->logger = $logger_channel;
    $this->serializer = $serializer;
    $this->eventDispatcher = $event_dispatcher;
    $this->api = $coinbase_api;
    $this->timeService = $time_service;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('logger.factory')->get('coinbase_webhooks'),
      $container->get('serializer'),
      $container->get('event_dispatcher'),
      $container->get('commerce_coinbase.api'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Builds the response.
   */
  public function handleIncomingWebhook(PaymentGatewayInterface $commerce_payment_gateway, Request $request) {
    if ($commerce_payment_gateway === NULL) {
      $this->logger->error('Empty payment gateway');
      throw new UnprocessableEntityHttpException('Payment gateway does not exist.');
    }

    $payload = $request->getContent();
    if (!$payload) {
      $this->logger->error('Empty payload');
      return new Response();
    }
    \Drupal::logger('coinbase')->alert('Payload:<pre><code>' . print_r($payload, TRUE) . '</code></pre>');
    $data = $this->serializer->decode($payload, 'json');
    if (!$data) {
      $this->logger->error('Empty json');
      return new Response();
    }
    \Drupal::logger('coinbase')->alert('Data:<pre><code>' . print_r($data, TRUE) . '</code></pre>');
    $type = $data['event']['type'] ?? '';
    $order_id = $data['event']['data']['metadata']['order_id'] ?? '';
    $this->api->log('', 'webhook:' . $type, $request, ['order_id' => $order_id]);

    $hash_request = $request->headers->get('X-CC-Webhook-Signature');
    if (!$hash_request) {
      $this->logger->error('Empty Webhook-Signature header in request');
      return new Response();
    }
    $secret = $commerce_payment_gateway->getPluginConfiguration()['secret'] ?? '';
    if (!$secret) {
      $this->logger->error('Empty webhook secret. Should be configured first');
      throw new BadRequestHttpException('Webhook secret should be configured');
    }
    $hash_calculated = hash_hmac('sha256', $payload, $secret);
    // $hash_calculated = base64_encode($hash_calculated);
    if ($hash_calculated != $hash_request) {
      $this->logger->error('Hmac validation failed @request != @check<pre><code>' . print_r($data, TRUE) . '</code></pre>', [
        '@request' => $hash_request,
        '@check' => $hash_calculated,
      ]);
      return new Response();
    }

    if (empty($order_id)) {
      $this->logger->error('Empty metadata.order_id. Details in log');
      return new Response();
    }
    $order = $this->entityTypeManager->getStorage('commerce_order')
      ->load($order_id);
    if (!$order instanceof OrderInterface) {
      $this->logger->error('Order #@id do not exists', ['@id' => $order_id]);
      return new Response();
    }

    $payment = $this->getPayment($order->id());

    if ($payment instanceof PaymentInterface && $payment->getState()->getId() === 'completed') {
      $this->logger->error('Order #@id already paid. Event: @type', [
        '@id' => $order_id,
        '@type' => $type,
      ]);
      return new Response();
    }

    if (!$payment) {
      $payment = $this->createPayment($order, $commerce_payment_gateway);
      $payment->setRemoteId($data['event']['data']['code']);
    }
    $payment->set('remote_state', $type);
    $payment->save();

    if ($type === 'charge:confirmed') {
      // Let's collect paid amount
      $paid_amount = 0;
      foreach ($data['event']['data']['payments'] as $payment_income) {
        $paid_amount = Calculator::add($paid_amount, $payment_income['value']['local']['amount']);
      }
      // Finalize the payment.
      $payment->setState('completed');
      $payment->setAmount(new Price($paid_amount, $payment->getAmount()->getCurrencyCode()));
      $request_time = $this->timeService->getRequestTime();
      $payment->setCompletedTime($request_time);
      $payment->save();

      // Finalize the payment.
      if ($order->getState()->isTransitionAllowed('place')) {
        $event = new OrderEvent($order);
        $this->eventDispatcher->dispatch(CheckoutEvents::COMPLETION, $event);
        $order->getState()->applyTransitionById('place');
        $order->save();
      }
    }
    return new Response();
  }


  /**
   * Finds the payment by oder_id.
   *
   * @param string $order_id
   *   The payment method remote ID.
   *
   * @return \Drupal\Core\Entity\EntityInterface|array
   *   The entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getPayment(string $order_id) {
    $payments = $this->entityTypeManager
      ->getStorage('commerce_payment')
      ->loadByProperties([
        'order_id' => $order_id,
      ]);

    return $payments ? reset($payments) : [];
  }

  /**
   * Creates a payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The payment.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createPayment(OrderInterface $order, PaymentGatewayInterface $payment_gateway) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entityTypeManager->getStorage('commerce_payment')
      ->create([
        'amount' => $order->getBalance(),
        'payment_gateway' => $payment_gateway->id(),
        'order_id' => $order->id(),
        'remote_id' => '',
        'remote_state' => '',
      ]);

    $payment->save();

    // Add payment details to the order.
    $order->set('payment_gateway', $payment->getPaymentGateway());
    $order->save();

    return $payment;
  }

}
