<?php

namespace Drupal\commerce_coinbase\PluginForm\OffsiteRedirect;

use Drupal\commerce_coinbase\CoinbaseApi;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PaymentOffsiteForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * @var \Drupal\commerce_coinbase\CoinbaseApi
   */
  protected $client;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(CoinbaseApi $coinbaseApi, AccountProxyInterface $current_user, Token $token, EntityTypeManagerInterface $entity_type_manager) {
    $this->client = $coinbaseApi;
    $this->currentUser = $current_user;
    $this->token = $token;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_coinbase.api'),
      $container->get('current_user'),
      $container->get('token'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $order = $payment->getOrder();

    if ($order && !$order->getOrderNumber()) {
      $order_type_storage = $this->entityTypeManager->getStorage('commerce_order_type');
      /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
      $order_type = $order_type_storage->load($order->bundle());
      /** @var \Drupal\commerce_number_pattern\Entity\NumberPatternInterface $number_pattern */
      $number_pattern = $order_type->getNumberPattern();
      if ($number_pattern) {
        $order_number = $number_pattern->getPlugin()->generate($order);
      }
      else {
        $order_number = $order->id();
      }

      $order->setOrderNumber($order_number);
      $order->save();
    }

    $api_key = $payment_gateway_plugin->getConfiguration()['api_key'];
    $charge_name = $payment_gateway_plugin->getConfiguration()['charge_name'];
    if (!$charge_name) {
      $charge_name = 'Order #[commerce_order:order_id]';
    }
    $charge_name = $this->token->replace($charge_name, [
      'commerce_order' => $order,
    ]);
    $charge_description = $payment_gateway_plugin->getConfiguration()['charge_description'];
    if (!$charge_description) {
      $charge_description = 'Order #[commerce_order:order_id] at [site:name]';
    }
    $charge_description = $this->token->replace($charge_description, [
      'commerce_order' => $order,
    ]);

    $this->client->setApiKey($api_key);
    $params = [
      'name' => mb_substr($charge_name, 0, 100),
      'description' => mb_substr($charge_description, 0, 200),
      'pricing_type' => 'fixed_price',
      'local_price' => [
        'amount' => $payment->getAmount()->getNumber(),
        'currency' => $payment->getAmount()->getCurrencyCode(),
      ],
      'metadata' => [
        'customer_id' => $this->currentUser->id(),
        'order_id' => $order->id(),
        'order_number' => $order->getOrderNumber(),
      ],
      'redirect_url' => $form['#return_url'],
      'cancel_url' => $form['#cancel_url'],
    ];
    $coinbase_data = $this->client->createCharge($params);
    $redirect_url = $coinbase_data['hosted_url'] ?? '';
    if (!$redirect_url) {
      throw new Exception('Error create payment');
    }

    // $redirect_url = Url::fromRoute('commerce_coinbase.redirect', [], ['absolute' => TRUE])->toString();
    $form = $this->buildRedirectForm($form, $form_state, $redirect_url, []);

    // Disable the javascript that auto-clicks the Submit button.
    unset($form['#attached']['library']);

    return $form;
  }

}
