<?php

namespace Drupal\commerce_coinbase;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validation;

/**
 * Helper service.
 */
class CoinbaseApi {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * @var string
   */
  private $apikey;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a Helper object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Database\Connection $connection
   * @param \Drupal\Core\Session\AccountInterface $current_user
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   */
  public function __construct(ClientInterface $http_client, LoggerChannelFactoryInterface $logger, Connection $connection, AccountInterface $current_user, RequestStack $request_stack) {
    $this->httpClient = $http_client;
    $this->logger = $logger->get('commerce_coinbase');
    $this->connection = $connection;
    $this->currentUser = $current_user;
    $this->request = $request_stack;
  }

  public function setApiKey($apikey) {
    $this->apikey = $apikey;
  }

  protected function getResponseBody($response) {
    $result = '';
    if (is_object($response)) {
      if ($response instanceof ResponseInterface) {
        $result = (string) $response->getBody();
      }
      elseif ($response instanceof Request) {
        $result = (string) $response->getContent();
      }
    }
    elseif (is_scalar($response)) {
      $result = (string) $response;
    }
    return $result;
  }

  /**
   * @param $params
   *
   * @return array{
   *   name: string,
   *   description: string,
   *   pricing_type: string,
   *   local_price: array{amount: string, currency: string},
   *   request_info: array}|FALSE
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function createCharge($params) {
    $params['pricing_type'] ??= 'fixed_price';
    if (!$this->validateChargeParams($params)) {
      return FALSE;
    }

    try {
      $response = $this->httpClient->request('POST', 'https://api.commerce.coinbase.com/charges', [
        'headers' => [
          'X-CC-Api-Key' => $this->apikey,
          'X-CC-Version' => '2018-03-22',
        ],
        'json' => $params,
      ]);
    }
    catch (ClientException $e) {
      $data = $this->getResponseBody($e->getResponse());
      $this->logger->error('Create checkout failed: @msg. Debug: @data', [
        '@msg' => $e->getMessage(),
        '@data' => $data,
      ]);
      throw $e;
    }

    $data = $this->getResponseBody($response);
    if ($response->getStatusCode() != 201) {
      $this->logger->error('Response:@code Message:@msg. Debug: @data', [
        '@code' => $response->getStatusCode(),
        '@msg' => $response->getReasonPhrase(),
        '@data' => $data,
      ]);
      throw new \Exception('Request to Coinbase service failed');
    }
    $json = json_decode($data, TRUE);
    if (empty($json)) {
      throw new \Exception('Response from Coinbase service is empty');
    }
    return $json['data'] ?? FALSE;
  }

  /**
   * Validate parameters for create a checkout
   *
   * @see https://commerce.coinbase.com/docs/api/#create-a-checkout
   *
   * @param $params
   *
   * @return bool
   */
  private function validateCheckoutParams($params) {
    if (($params['pricing_type'] ?? '') == 'fixed_price') {
      $local_price = [
        'amount' => [
          new Assert\NotBlank(),
          new Assert\Positive(),
        ],
        // It's require symfony/intl package
        // 'currency' => new Assert\Currency(),
        'currency' => [
          new Assert\NotBlank(),
          new Assert\Length(3),
        ],
      ];
    }
    else {
      $local_price = new Assert\Optional();
    }
    // @codingStandardsIgnoreStart
    $constraints = new Assert\Collection([
      'fields' => [
        'name' => new Assert\Length([
          'max' => 100,
        ]),
        'description' => new Assert\Length([
          'max' => 200,
        ]),
        'pricing_type' => new Assert\Choice(['fixed_price', 'no_price']),
        'local_price' => new Assert\Collection($local_price),
        'requested_info' => new Assert\Optional([
          new Assert\Type('array'),
          new Assert\Choice(['name', 'email'])
        ]),
      ],
    ]);
    // @codingStandardsIgnoreEnd

    $validator = Validation::createValidator();
    $violations = $validator->validate($params, $constraints);

    if ($violations->count() > 0) {
      $this->logValidationErrors($violations, $params);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Validate parameters for create a charge
   *
   * @see https://commerce.coinbase.com/docs/api/#create-a-charge
   *
   * @param $params
   *
   * @return bool
   */
  private function validateChargeParams($params) {
    if (($params['pricing_type'] ?? '') == 'fixed_price') {
      $local_price = [
        'amount' => [
          new Assert\NotBlank(),
          new Assert\Positive(),
        ],
        // It's require symfony/intl package
        // 'currency' => new Assert\Currency(),
        'currency' => [
          new Assert\NotBlank(),
          new Assert\Length(3),
        ],
      ];
    }
    else {
      $local_price = new Assert\Optional();
    }
    // @codingStandardsIgnoreStart
    $constraints = new Assert\Collection([
      'fields' => [
        'name' => new Assert\Length([
          'max' => 100,
        ]),
        'description' => new Assert\Length([
          'max' => 200,
        ]),
        'pricing_type' => new Assert\Choice(['fixed_price', 'no_price']),
        'local_price' => new Assert\Collection($local_price),
        'metadata' => new Assert\Optional(),
        'redirect_url' => new Assert\Optional([
          new Assert\Url(),
        ]),
        'cancel_url' => new Assert\Optional([
          new Assert\Url(),
        ]),
      ],
    ]);
    // @codingStandardsIgnoreEnd

    $validator = Validation::createValidator();
    $violations = $validator->validate($params, $constraints);

    if ($violations->count() > 0) {
      $this->logValidationErrors($violations, $params);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @param \Symfony\Component\Validator\ConstraintViolationListInterface $violations
   * @param $params
   *
   * @return void
   */
  public function logValidationErrors(\Symfony\Component\Validator\ConstraintViolationListInterface $violations, $params): void {
    $message = implode("\n", array_map(static function (ConstraintViolationInterface $violation) {
      return $violation->getPropertyPath()
        . ': '
        . PlainTextOutput::renderFromHtml($violation->getMessage());
    }, (array) $violations->getIterator()));

    $this->logger->error('Unprocessable Coinbase Params: validation failed: %message. Debug: <pre><code>{debug}</code></pre>', [
      '%message' => $message,
      'debug' => print_r($params, TRUE),
    ]);
  }

  /**
   * Save record to log.
   *
   * @param string $url
   *   The url for request.
   * @param string $type
   *   The type of record: request/response/webhook.
   * @param array|object $data
   *   Array of post data for request or response body.
   * @param array $log_parameters
   *   Extra parameters for store in the log.
   */
  public function log($url, $type, $data, array $log_parameters = []) {
    try {
      if (is_object($data)) {
        if ($data instanceof ResponseInterface) {
          $data = [
            'code' => $data->getStatusCode(),
            'headers' => $data->getHeaders(),
            'body' => (string) $data->getBody(),
          ];
        }
        elseif ($data instanceof Request) {
          $data = [
            'code' => NULL,
            'headers' => $data->headers,
            'body' => (string) $data->getContent(),
          ];
        }
      }
      $this->connection
        ->insert('commerce_coinbase_log')
        ->fields(
          [
            'uid' => $this->currentUser->id(),
            'hostname' => mb_substr($this->request->getCurrentRequest()?->getClientIP(), 0, 128),
            'created' => time(),
            'url' => substr($url, 0, 1000),
            'type' => substr($type, 0, 64),
            'data' => serialize($data),
            'order_id' => $log_parameters['order_id'] ?? '',
          ]
        )
        ->execute();
    }
    catch (\Exception $e) {
      $this->logger->error(sprintf('Failed log %s. url: %s. Message: %s. Data: <pre><code>%s</code></pre>', $type, $url, $e->getMessage(), print_r($data, TRUE)));
      // Do nothing. Main goail is prevent any notify.
    }
  }

}
